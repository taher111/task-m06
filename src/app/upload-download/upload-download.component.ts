import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelpermethodsService } from '../Services/helper-method.service';

@Component({
  selector: 'app-upload-download',
  templateUrl: './upload-download.component.html',
  styleUrls: ['./upload-download.component.scss']
})
export class UploadDownloadComponent {

  url: any;
  selectedFile: File = null;

  constructor(private http: HttpClient, public helperMethod: HelpermethodsService) {
    this.url = "https://api.cmm.test.m06.cloud/";
  }

  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
  }

  onUpload() {
    const fd = new FormData();
    this.helperMethod.presentLoading();
    fd.append('image', this.selectedFile, this.selectedFile.name);
    this.http.post(`${this.url}api/v1/file/upload`, fd).subscribe((res: any) => {
      console.log(res);
      this.helperMethod.dismissLoading();
    }, err => {
      this.helperMethod.dismissLoading();
      this.helperMethod.presentToast('Something went wrong please try again later');
    });
  }
}
