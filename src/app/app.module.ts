import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UploadDownloadComponent } from './upload-download/upload-download.component';
import { HelpermethodsService } from './Services/helper-method.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { InterceptorProvider } from './Services/interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    UploadDownloadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [HelpermethodsService, { provide: HTTP_INTERCEPTORS, useClass: InterceptorProvider, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
