import { HttpClient, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class InterceptorProvider implements HttpInterceptor {

  constructor(public http: HttpClient) {
    console.log('Hello InterceptorProvider Provider');
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('/api/login/')) {
      return next.handle(req).pipe(map(event => {
        if (event instanceof HttpResponse) {

          if (event.body.token) {
            const token = event.body.token;
            localStorage.setItem('token', token)
          }
          return event;
        }
      }))
    }
    else {


      if (req.url.includes('/api/default/')) {
        console.log('not auth in interceptor');
        req = req.clone({ setHeaders: { 'Authorization': `Token 24e163498b1fba68f3e6ead715575316a8a24812` } });
        req = req.clone({ setHeaders: { 'Content-Type': `application/json` } });
        return next.handle(req)

      }

      else {
        const token = localStorage.getItem('token');
        if (token) {
          console.log('interceptor token found')
          req = req.clone({ setHeaders: { 'Authorization': `Token ${token}` } });
          req = req.clone({ setHeaders: { 'Content-Type': `application/json` } });

          //console.log('language: ', localStorage.getItem('lang'));

          return next.handle(req);
        } else {
          console.log('interceptor token not found');
          req = req.clone({ setHeaders: { 'Authorization': `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIn0.NRw5tZsd_ZJhWF1hW45ku8WEVvOkj8SzHCu439QvZPoP_y5XPKvp39pxrjddeI-Y69LsaBEGoUczotQGtt9KwA ` } });
          req = req.clone({ setHeaders: { 'Content-Type': `application/json` } });
          return next.handle(req)
        }
      } //else
      // } //else
    }
  }
}

